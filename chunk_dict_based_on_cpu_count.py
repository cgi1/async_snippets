"""
@author: CGi1
"""

from itertools import islice

def chunks(data, SIZE=10):
    it = iter(data)
    for i in range(0, len(data), SIZE):
        yield {k:data[k] for k in islice(it, SIZE)}


def execute_code_per_symbol_in_async_processes(my_dict_of_dicts):
    no_of_processes = 1
    no_of_processes = mp.cpu_count() - 1 or 1
    chunk_size = int(len(my_dict_of_dicts) / no_of_processes)
    my_dict_of_dicts_chunks = chunks(data=my_dict_of_dicts, SIZE=chunk_size)

    for my_dict_of_dicts_chunk in my_dict_of_dicts_chunks:
        print("Start process %s" % i)
            worker_process = mp.Process(target=run_code_per_chunk, args=(my_dict_of_dicts_chunk),
                                        daemon=True,
                                        name='worker_process_{}'.format(i))
            worker_process.start()
            processes.append(worker_process)
            
    for process in processes:
        process.join()

    print("Continue synchronized")
