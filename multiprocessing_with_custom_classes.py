"""
@author: CGi1
"""

import time
import random
import multiprocessing as mp

def my_function(arg1, global_obj, arg2, my_name, my_data, my_class, q):

    print("Start my_function")
    stime = random.randint(5,10)
    print("sleep %s" % stime)
    time.sleep(stime)
    print("Exec after %s" % stime)

    put_dict = {
        'my_name': my_name,
        'stime': stime
    }
    print('PUT %s' % put_dict)
    q.put(put_dict)

    return arg1

class MySecondClass():

    my_dict = {
        'michael': 'jobst',
        'jan': 'Go'
    }
    def get_my_dict(self):
        return self.my_dict

class MyClass():

    test_var = 0
    my_second_class = None

    def __init__(self):
        self.test_var = random.random()
        self.my_second_class = MySecondClass()

    def get_test_var(self):
        print(self.test_var)
        return [self.test_var]

    def get_second_class(self):
        return self.my_second_class

def process_queue_data(received):
    print("RECV %s" % received)


global_obj = {
    'name1': 'value1',
    'name2': 'value2',
    'name3': 'value3',
    'name4': 'value4'
}

my_class = MyClass()

arg1 = 1
arg2 = 2

q = mp.Queue()

processes = []
no_of_processes = 20
for my_name in range(no_of_processes):
    print("Start process %s" % my_name)
    my_data = my_name*100
    worker_process = mp.Process(target=my_function, args=(arg1, global_obj, arg2, my_name, my_data, my_class, q), daemon=True,
                                name='worker_process_{}'.format(my_name))
    worker_process.start()
    processes.append(worker_process)



while 1:
    running = any(p.is_alive() for p in processes)
    while not q.empty():
        process_queue_data(q.get())
    if not running:
        print('All processes finished!')
        break
    import time
    timel.sleep(0.1)

for process in processes:
    process.join()

# Wait for all processes to end
print ("Continue synchronized")

# Read from mp.Queue()
while not q.empty():
    print(q.get())

